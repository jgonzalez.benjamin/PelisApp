import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { MovieComponent } from './components/movie/movie.component';
import { SearchComponent } from './components/search/search.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MoviesGaleryComponent } from './components/movies-galery/movies-galery.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'movie/:id/:pag', component: MovieComponent },
    { path: 'movie/:id/:pag/:search', component: MovieComponent },
    { path: 'movies', component: MoviesGaleryComponent},
    { path: 'search', component: SearchComponent },
    { path: 'search/:text', component: SearchComponent },
    { path: 'not-found', component: PageNotFoundComponent },
    { path: '**', pathMatch: 'full' ,redirectTo: 'not-found' },

    //{ path: 'path/:routeParam', component: MyComponent },
    //{ path: 'staticPath', component: ... },
    //{ path: '**', component: ... },
    //{ path: 'oldPath', redirectTo: '/staticPath' },
    //{ path: ..., component: ..., data: { message: 'Custom' }
]

export const APP_ROUTING = RouterModule.forRoot(routes);
