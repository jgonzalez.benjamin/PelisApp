import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { MoviesService } from '../../services/movies.service';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styles: []
})
export class MovieComponent implements OnInit {

  movie:any;
  backTo:string = '';
  search:string = '';

  constructor( public _mService:MoviesService, public _aRouter:ActivatedRoute, public _router:Router) { 

    this._aRouter.params.subscribe(
      params => {
        
        if(params['search']){
          this.search = params['search'];
        }
        this.backTo = params['pag'];
        this._mService.getMovie(params['id']).subscribe(movieDb => {
          this.movie = movieDb; 
        });

        
      } 
    )
  }

  ngOnInit() {
  }

  back(){
    this._router.navigate(['/',this.backTo]);
  }

}
