import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from '../../services/movies.service';
import { log } from 'util';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [ './navbar.component.scss' ]
})
export class NavbarComponent implements OnInit {
  search:string = '';
  constructor( public _router:Router, public _mService:MoviesService ) { }

  ngOnInit() {
  }

  searchMovie(text:string){
    console.log(text);
    this.search = text;
    
    if(text.length == 0){
      return;
    }
  
    this._router.navigate(['search',text]);
  }
  getMovieYear(year:number){
    this._mService.getBestMovieYear(year).subscribe( 
      res => console.log(res)
      
    );
  }

}
