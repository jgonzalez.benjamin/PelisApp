import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  movies: any;
  moviesKids:any;
  year:number = 2017;
  moviesOfTheYear:any;

  constructor( private _moviesService: MoviesService ) { 
    this._moviesService.getPopular().subscribe( data => this.movies = data.results );
    this._moviesService.getPopularKids().subscribe( data => this.moviesKids = data.results);
    this._moviesService.getBestMovieYear(this.year).subscribe( data => this.moviesOfTheYear = data.results);
    this._moviesService.getMovieWithVideo().subscribe(
      res => {
        console.log(res);
        
      }
    )
   }

  ngOnInit() {
  }

}
