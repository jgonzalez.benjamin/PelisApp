import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-movies-galery',
  templateUrl: './movies-galery.component.html',
  styleUrls: ['./movies-galery.component.scss']
})
export class MoviesGaleryComponent implements OnInit {

  @Input('movies') moviesGallery;
  @Input('title') title;

  constructor() { }

  ngOnInit() {
  }

}
