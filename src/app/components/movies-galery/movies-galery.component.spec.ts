import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesGaleryComponent } from './movies-galery.component';

describe('MoviesGaleryComponent', () => {
  let component: MoviesGaleryComponent;
  let fixture: ComponentFixture<MoviesGaleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesGaleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesGaleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
