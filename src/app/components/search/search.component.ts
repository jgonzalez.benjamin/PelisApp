import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  search:string = "";
  genres:any[];
  constructor( public _mService:MoviesService, public _aRouter:ActivatedRoute, public _router:Router ) {
    this._aRouter.params.subscribe( res => {
      console.log(res['text']);
      
        if(res['text']){
          this.search = res['text'];
          this.searchMovie(this.search);
        }
    })
   }

  ngOnInit() {
  }

  searchMovie( text:string ){
    this.search = text;

    if (this.search.length == 0) {
      console.log(this.search);
      
      return;
    }
    
    this._mService.searchMovie(this.search).subscribe();
  }

  thisMovie(id:string){
    this._router.navigate(['movie',id,'search']);
  }

}
