import { Injectable } from '@angular/core';
import { Jsonp } from "@angular/http";
import 'rxjs/Rx';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private apikey:string = '3953ce57574ed8a911406bd8d60cecde';
  private urlMovieDb:string = 'https://api.themoviedb.org/3';

  movies:any[] = [];
  index_pages:number;

  constructor( private _jsonp:Jsonp ) { }

  getPopular(){
    let url = `${this.urlMovieDb}/discover/movie?sort_by=popularity.desc&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }
  getBestMovieYear( year:number ){
    let url = `${this.urlMovieDb}/discover/movie?include_video=true&primary_release_year=${year}&sort_by=vote_average.desc&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }
  getMoviesInTheatres( date:string, postDate:string ){
    let url = `${this.urlMovieDb}/discover/movie?primary_release_date.gte=${date}&primary_release_date.lte=${postDate}&language=es&callback=JSONP_CALLBACK`;
    
    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }
  getPopularKids(){
    let url = `${this.urlMovieDb}/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;
    
    return this._jsonp.get(url).pipe(map(res=>res.json()));

  }
  searchMovie( text:string ){
    let url = `${this.urlMovieDb}/search/movie?query=${text}&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url).pipe(map(
      res=>{
        this.movies = res.json().results;
        return res.json();
      }
    ));
  }
  getGenres(){
    let url = `${this.urlMovieDb}/genre/movie/list?language=en-US&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;
    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }
  getMovie(id:string){

    let url = `${this.urlMovieDb}/movie/${id}?api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;
    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }
  getMovieWithVideo(){
    let url = `${this.urlMovieDb}/discover/movie?page=1&include_video=true&sort_by=popularity.desc&language=en-US&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;
    return this._jsonp.get(url).pipe(map(res=>res.json()));
  }

}
