import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JsonpModule, HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { MovieComponent } from './components/movie/movie.component';

import { MoviesService } from './services/movies.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SearchComponent } from './components/search/search.component';
import { APP_ROUTING } from './app.routes';
import { MoviesGaleryComponent } from './components/movies-galery/movies-galery.component';
import { MoviesImagePipe } from './pipes/movies-image.pipe';
import { GenrePipe } from './pipes/genre.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    MovieComponent,
    PageNotFoundComponent,
    SearchComponent,
    MoviesGaleryComponent,
    MoviesImagePipe,
    GenrePipe
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    HttpModule,
    RouterModule,
    APP_ROUTING
  ],
  providers: [
    MoviesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
